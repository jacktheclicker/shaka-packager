PlayReady encryption options
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

--enable_playready_encryption

    Enable encryption with PlayReady key. This generates PlayReady protection
    system. Additional protection systems can be generated with
    --additional_protection_systems option.

--playready_server_url <url>

    PlayReady packaging server url.

--program_identifier <program_identifier>

    Program identifier for packaging request.

--ca_file <file path>

    Absolute path to the certificate authority file for the server cert.
    PEM format.

--client_cert_file <file path>

    Absolute path to client certificate file.

--client_cert_private_key_file <file path>

    Absolute path to the private key file.

--client_cert_private_key_password <string>

    Password to the private key file.
